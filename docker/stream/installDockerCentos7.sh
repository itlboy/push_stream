yum -y update

tee /etc/yum.repos.d/docker.repo <<-'EOF'
[dockerrepo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/7/
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
EOF

yum -y install docker-engine

service docker start;
        $eventId = $decodedBody['eventId'];
        $eventCreated = $decodedBody['eventCreated'];
        $storeId = $decodedBody['storeId'];
        $entityId = $decodedBody['entityId'];
        $eventType = $decodedBody['eventType'];
        $data = ​$decodedBody['data'];