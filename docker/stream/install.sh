#docker run -it -p 8080:8080 -p 1935:1935 --name stream stream /bin/bash
#stream
# ffmpeg -loglevel verbose -re -i sample.mp4  -vcodec libx264 -vprofile baseline -acodec \
# libmp3lame -ar 44100 -ac 1 -f flv rtmp://s.videotructiep.com:1935/live/kien
#update yum
yum -y update
#install nano 
yum -y install nano
#install dev tools
yum -y install gcc gcc-c++ make zlib-devel pcre-devel openssl-devel
#install unzip
yum -y install unzip
#install wget
yum -y install wget
#setup nginx service
yum -y install initscripts
#install httptools
yum install -y httpd-tools
#install nscd
yum -y install nscd
#install xz
yum -y install xz


cd  /root
#download nginx version 1.11.3

wget http://nginx.org/download/nginx-1.11.3.tar.gz
tar -xzf nginx-1.11.3.tar.gz 
ln -sf nginx-1.11.3 nginx

#download rtmp
wget https://github.com/arut/nginx-rtmp-module/archive/master.zip
unzip master.zip
cd  nginx

#install nginx
./configure \
--user=root                          \
--group=root                         \
--prefix=/etc/nginx                   \
--sbin-path=/usr/sbin/nginx           \
--conf-path=/etc/nginx/nginx.conf     \
--pid-path=/var/run/nginx.pid         \
--lock-path=/var/run/nginx.lock       \
--error-log-path=/var/log/nginx/error.log \
--http-log-path=/var/log/nginx/access.log \
--with-http_gzip_static_module        \
--with-http_stub_status_module        \
--with-http_ssl_module                \
--with-pcre                           \
--with-file-aio                       \
--with-http_realip_module             \
--without-http_scgi_module            \
--without-http_uwsgi_module           \
--without-http_fastcgi_module		    \
--cp-module=../nginx-rtmp-module-master
make
make install

#cp authen pass for nginx
htpasswd -bc /etc/nginx/.htpasswd stream  khongthetinnoi
#cp nginx configs
mkdir /etc/nginx/conf.d
cp nginx.conf /etc/nginx/nginx.conf
cp rtmp.conf /etc/nginx
cp rtmpHttp.conf /etc/nginx/conf.d
cp stat.xsl /etc/nginx

wget -O /etc/init.d/nginx https://gist.github.com/sairam/5892520/raw/b8195a71e944d46271c8a49f2717f70bcd04bf1a/etc-init.d-nginx
chmod +x /etc/init.d/nginx
chkconfig --cp nginx
chkconfig --level 345 nginx on

## download ffmpeg
cd  /root
wget http://johnvansickle.com/ffmpeg/builds/ffmpeg-git-64bit-static.tar.xz
unxz ffmpeg-git-64bit-static.tar.xz
tar -xf ffmpeg-git-64bit-static.tar -C /usr/bin --strip-components=1

## remove files
rm -Rf master.zip nginx-1.11.3 nginx nginx-1.11.3.tar.gz nginx-rtmp-module-master ffmpeg-git-64bit-static.tar.xz ffmpeg-git-64bit-static.tar install.log.syslog install.log


