NAME="stream"
docker run -it -d -P -v /root:/home/share -p 8080:80 -p 2222:22 -p 1935:1935 --name $NAME itlboy/stream /bin/bash
docker exec -it $NAME  service nginx start
docker exec -it $NAME  service sshd start
docker exec -it $NAME  /bin/bash